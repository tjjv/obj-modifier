import java.nio.FloatBuffer;

public class ObjModifier {

    private final static float MIN_DISTANCE_TO_MOVE = 10E-7f;

    /**
     * Center the model in FloatBuffer
     * <p>
     * We often get 3D models which are not positioned at the origin of the coordinate system. Placing such models on a
     * 3D map is harder for users. One solution is to reposition such a model so that its center is at the origin of the
     * coordinate system.
     * </p>
     * 
     * @param buffer FloatBuffer storing the vertices of a mesh in a continuous array of floats:<br>
     *               <p>
     *               {@code [x0, y0, z0, x1, y1, z1, ..., xn, yn, zn]}
     *               </p>
     *               This function is not validating the input buffer. It needs to be validated before centering, if it
     *               is coming from an unreliable source.
     * 
     */
    public static void centerVertices(FloatBuffer buffer) {
	float[] originalCenterPosition = getCenterVector(buffer);
	moveBufferToOrigin(buffer, originalCenterPosition);
    }

    // Position of center. In this case the center is middle point between min and max coordinate components.
    private static float[] getCenterVector(FloatBuffer buffer) {
	float[] min = new float[3];
	float[] max = new float[3];

	buffer.get(min, 0, 3);
	buffer.get(max, 0, 3);

	int coordinateIndex = 0;
	if (buffer.capacity() > 3) {
	    buffer.rewind();
	    while (buffer.hasRemaining()) {
		float value = buffer.get();
		if (value < min[coordinateIndex]) {
		    min[coordinateIndex] = value;
		} else if (value > max[coordinateIndex]) {
		    max[coordinateIndex] = value;
		}
		coordinateIndex = (coordinateIndex + 1) % 3;
	    }
	}

	return new float[] { 
		0.5f * (min[0] + max[0]), 
		0.5f * (min[1] + max[1]), 
		0.5f * (min[2] + max[2]) 
	};
    }

    // Moves buffer from given center to origin.
    // Each coordinate component values are modified only if component center distance from origin is greater than MIN_DISTANCE_TO_MOVE.
    private static void moveBufferToOrigin(FloatBuffer buffer, float[] originalCenterVector) {
	for (int coordinateIndex = 0; coordinateIndex <= 2; coordinateIndex++) {
	    float originalPosition = originalCenterVector[coordinateIndex];
	    float distanceToMove = -1 * originalPosition;
	    if (Math.abs(distanceToMove) > MIN_DISTANCE_TO_MOVE) {
		moveBufferCoordinateComponentValues(buffer, distanceToMove, coordinateIndex);
	    }
	}
    }

    // Moves all values of single coordinate component with given distance
    private static void moveBufferCoordinateComponentValues(FloatBuffer buffer, float distanceToMove,
	    int coordinateComponentIndex) {
	for (int bufferIndex = coordinateComponentIndex; bufferIndex < buffer.capacity(); bufferIndex += 3) {
	    buffer.put(bufferIndex, buffer.get(bufferIndex) + distanceToMove);
	}
    }

}
